How to package it as .ipk?
https://www.webosose.org/docs/tools/sdk/cli/cli-user-guide/#ares-package

Creating a package file from the ./sampleApp directory and outputting it in the ./output directory

ares-package -o output sampleApp

Ex: ares-package ~/samples/sampleApp ~/samples/sampleService

To include partner certificate and key.

EX: ares-package -s ~/Project/lghwa/PARTNER_Prv.key -crt ~/Project/lghwa/partner_certificate.crt -o ~/Project/lghwa/ ~/Project/lghwa/